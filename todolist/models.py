from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoItem(models.Model):
    # CharField = String value
    # DateTimeField = datetime datatype
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    status = models.CharField(max_length = 50, default = "Pending")
    date_created = models.DateTimeField("Date Created")
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


class Events(models.Model):
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    status = models.CharField(max_length=50, default="Upcoming")
    event_date = models.DateTimeField("Event date")
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, default="")
    