from django.urls import path
from . import views

app_name = 'todolist'
urlpatterns = [
    path('', views.index, name="index"),
    # localhost:8000/todolist/1
    path('<int:todoitem_id>/',views.todoitem,name="viewitem"),
    path('register/',views.register,name="register"),
    path('change-password/',views.change_password,name="change_password"),
    path('login/',views.login_view,name="login"),
    path('logout/',views.logout_view,name="logout"),
    path('add-task/',views.add_task,name="add_task"),
    path('<int:todoitem_id>/edit',views.update_task,name="update_task"),
    path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),
    path('add-event/',views.add_event,name="add_event"),
    path('view_event/<int:event_id>/',views.view_event,name="viewevent"),
    path('edit/<int:event_id>',views.update_event,name="update_event"),
    path('delete/<int:event_id>', views.delete_event, name="delete_event")
]